#!/bin/sh

#SBATCH --time=24:00:00
#SBATCH --ntasks=12
#SBATCH --mem-per-cpu=1G
#SBATCH -N-1 
#SBATCH --job-name=i-pi_300K_16b

source ~/i-pi/env.sh
module load scipy-stack

srun -n 1 i-pi input.xml > log & sleep 20
srun -n 8 ~/lammps/src/lmp_mpi -in in.hbeads & sleep 10
srun -n 1 ~/lammps/src/lmp_mpi -in in.hcontr & sleep 10
srun -n 2 ~/lammps/src/lmp_mpi -in in.full
